#!/usr/bin/python3.5
"""
 Exemplo de script que gera linhas csv com pontos de rastreio, de um grupo
 de veículos de um rótulo, em um determinado período

 bibliotecas extras usadas
 http://docs.python-requests.org/en/master/

 Api Rascol
 http://backend.rassystem.com.br/pages/api

 Analisado no pylint3 v1.5.2
"""

import csv
import sys
import requests

#Respeitar maiúsculas e minúsculas como estão no sistema
USER = 'seu usuario aqui'
PASSWORD = 'sua senha aqui'
NM_EMPRESA = 'nome da empresa no rascol'
NM_FILIAL = 'nome da filial no rascol'
NM_ROTULO = 'nome do rótulo no rascol'
#Intervalo de data hora q se deseja consultar
DATA_HORA_INICIO = '2010-07-11T08:00:00-03:00'
DATA_HORA_FIM = '2010-07-15T18:00:00-03:00'


def do_rascol_req(url_suffix):
    """ Encapsula o acesso http à api, fazendo o tratamento de erro dependendo
    do http code return https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
        Args:
            url_suffix: Api suffix, ver doc da Api Rascol
        Returns:
            json: Um Json ou GeoJSON com o resultado da chamada, ver docs da api
            https://en.wikipedia.org/wiki/JSON
            https://en.wikipedia.org/wiki/GeoJSON
    """
    try:
        request_result = requests.get(
            ('http://backend.rassystem.com.br/app/v1/api' + url_suffix), auth=(USER, PASSWORD))
        request_result.raise_for_status()  # Solta exceção se http code volta 4xx ou 5xx
    except requests.exceptions.HTTPError:
        print(request_result.status_code)
        print(request_result.text)
        raise
    return request_result.json()


def veiculos(nm_empresa, nm_filial, nm_rotulo):
    """ Retorna lista de veiculos do rotulo na filial/empresa (strings), em formato Json """
    idfilial = None
    for filial in do_rascol_req('/filiais'):
        if filial['empresa'] == nm_empresa and filial['nome'] == nm_filial:
            idfilial = filial['idFilial']
    if idfilial is None:
        raise Exception('Empresa %s, Filial %s não encontrada!' %
                        (nm_empresa, nm_filial))
    idrotulo = None
    for rotulo in do_rascol_req('/filiais/%s/rotulos' % (idfilial)):
        if rotulo['nome'] == nm_rotulo:
            idrotulo = rotulo['id']
    if idrotulo is None:
        raise Exception('Rotulo %s na Empresa %s, Filial %s não encontrada!' % (
            nm_rotulo, nm_empresa, nm_filial))
    return do_rascol_req('/rotulos/%s/veiculos' % (idrotulo))


def main():
    """ Loop principal """
    csv_stdout = csv.writer(sys.stdout, delimiter=',')
    for veiculo in veiculos(NM_EMPRESA, NM_FILIAL, NM_ROTULO):
        nome = veiculo['descricao']
        geojson_retorno = do_rascol_req('/veiculos/%s/rastreio?dataInicio=%s&dataFim=%s' %
                                        (veiculo['id'], DATA_HORA_INICIO, DATA_HORA_FIM))
        for geojson_ponto in geojson_retorno['features']:
            data = geojson_ponto['properties']['data']
            coordinate = geojson_ponto['geometry']['coordinates']
            csv_stdout.writerow((nome, data, coordinate[0], coordinate[1]))


if __name__ == "__main__":
    main()
